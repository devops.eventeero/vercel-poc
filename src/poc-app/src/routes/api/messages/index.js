import { variables } from '$lib/variables';

export async function get({ query }) {
    // get a query string
    var message = query.get('message');

    return {
        body: {
            message: message || 'Hello, world, ' + variables.myMessage
        }
    };
}